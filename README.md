# Levanta WordPress

Script de instalação de um novo site WordPress no servidor do LabIS. 
Escrito em Ansible para facilitar o uso via SSH.

## Requisitos

* Ansible 2.9.2
* Python 3.8.1

## Arquivos

* hosts - Arquivo que deve conter o endereço das máquinas-alvo
* vars.yml - Arquivo que deve conter as variáveis do script
* wp_install.yaml - Playbook (script) Ansible
* virtualhost_template.conf.j2 - Template do arquivo de configuração do Apache
* cria_banco.sql.j2 - Template do script de criação do banco de dados
* levanta_wordpress.sh - Script que lança o playbook

## Uso

Para executar baixar e executar esses scripts, execute os seguintes comandos dentro da pasta que contém esses scripts.

```bash
chmod +x levanta_wordpress.sh
./levanta_wordpress.sh
```
OBS: É necessário executar esses comandos de dentro da rede da UFRJ ou usando o servidor SSH do PESC
